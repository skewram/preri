# PRERI

---

## Programme de Régulation Énergétique Régional Intelligent

### Présentation

PRERI est un programme développé en Java destiné à effectué des prévisions sur la consommation électrique des ménages en France par région.

### Prérequis

> - Java 11+ ou OpenJDK 11+
> 
> - Une connexion internet au premier lancement

### Compilation

> gradle clear assembleDist


### Execution

Windows
> /build/distributions/PRERI-1.0-SNAPSHOT/bin/PRERI.bat

Linux / MacOS
> /build/distributions/PRERI-1.0-SNAPSHOT/bin/PRERI