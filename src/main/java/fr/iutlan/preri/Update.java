package fr.iutlan.preri;

import fr.iutlan.preri.DataExploitation.ConsummationParser;
import fr.iutlan.preri.DataExploitation.DataPath;
import javafx.concurrent.Task;
import javafx.scene.control.ProgressBar;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.CharBuffer;
import java.util.Scanner;
import java.util.zip.CRC32;

/**
 * Class to check if data are up to date or corrupted and download them
 * @see java.lang.Object
 * @see DataPath
 */
public class Update extends Task<Boolean> implements DataPath {
/*test caillou*/


    public boolean testConnection(){
        boolean isInternetOk = false;
        updateMessage("Checking internet connection");
        updateProgress(0, 100);
        System.out.println("Checking internet connection");
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(DataPath.base_url).openConnection();
            if(httpURLConnection.getResponseCode() == 200) {
                isInternetOk = true;
                updateMessage("Internet connection is ok");
                updateProgress(20, 100);
                System.out.println("Internet connection is ok");
            }
        } catch (Exception e) {
            updateMessage("Internet connection failed");
            updateProgress(20, 100);
            System.out.println("Internet connection failed");
        }return isInternetOk;
    }

    static void maj(){

    }
    /*fin des bêtises de caillou*/

    @Override
    protected Boolean call() throws Exception {
        if(testConnection()){
            /*
                Weather Update Checker
            */
            if(isUpdateAvailable(DataPath.weatherChecksumFile, DataPath.weather_base_url)){
                updateMessage("Update for weather data is available");
                updateProgress(30, 100);
                System.out.println("Mise a jour pour les donnees meteos disponible");
                downloadUpdate(DataPath.weather_base_url, DataPath.weather_csv_url, DataPath.weatherChecksumFile, DataPath.weatherFile_path);
                updateMessage("Update for weather data is available");
                updateProgress(30, 100);
                System.out.println("Mise a jour pour les donnees meteos disponible");
            }else{
                updateMessage("Weather data are up to date");
                updateProgress(50, 100);
                System.out.println("Donnees meteo a jour");
            }

            /*
                Data Update Checker
            */
            if(isUpdateAvailable(DataPath.checksumFile, DataPath.base_url)){
                updateMessage("Update for energy data is available");
                updateProgress(40, 100);
                System.out.println("Mise a jour pour les donnees energetiques disponible");
                downloadUpdate(DataPath.base_url, DataPath.csv_url, DataPath.checksumFile, DataPath.dataFile_path);
                createSignatureAndSerialize();
            }else{
                updateMessage("Energy data are up to date");
                updateProgress(60, 100);
                System.out.println("Donnees energie a jour");
                if(DataPath.signature.exists()) {
                    if(new Scanner(DataPath.signature).nextLong() == getRemoteCRC32(new File(DataPath.dataFile_path))) {
                        System.out.println("Data not corrupted");
                        updateMessage("Date not corrupted");
                        updateMessage("Parsing data...");
                        updateProgress(ProgressBar.INDETERMINATE_PROGRESS, 0);
                        MainFX.consummationParser = new ConsummationParser();
                    } else {
                        System.out.println("Data corrupted");
                        updateMessage("Data corrupted");
                        downloadUpdate(DataPath.base_url, DataPath.csv_url, DataPath.checksumFile, DataPath.dataFile_path);
                        createSignatureAndSerialize();
                    }
                }else{
                    System.out.println("Creating serialized file");
                    updateMessage("Creating serialized file");
                    createSignatureAndSerialize();
                }
            }
            /*je casse tout*/
            //        this.getFileCRC32()
        }else{
            System.out.println("Internet connection unavailable");
            updateMessage("Internet connection unavailable");
            if(DataPath.signature.exists()) {
                if (new Scanner(DataPath.signature).nextLong() == getRemoteCRC32(new File(DataPath.dataFile_path))) {
                    System.out.println("Data not corrupted");
                    updateMessage("Data not corrupted");
                    updateMessage("Parsing data...");
                    updateProgress(ProgressBar.INDETERMINATE_PROGRESS, 0);
                    MainFX.consummationParser = new ConsummationParser();
                } else {
                    System.out.println("Data corrupted");
                    /* Load LES CEREALES DE KAPI !!! */
                    try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(DataPath.cereal))){
                        System.out.println("Deserializing...");
                        updateMessage("Deserializing... (this operation can took a moment)");
                        MainFX.consummationParser = (ConsummationParser)ois.readObject();
                        System.out.println("les cereales success");
                        updateMessage("les cereales success");
                        System.out.println(MainFX.consummationParser.getEnergyData().size());
                    }catch(ClassNotFoundException | IOException e){
                        e.printStackTrace();
                    }
                }
            }else{
                System.err.println("Can't know if data are corrupted or not use them at your own risk !");
                updateMessage("Can't know if data are corrupted or not use them at your own risk !");
                MainFX.consummationParser = new ConsummationParser();
            }
        }
        return Boolean.TRUE;
    }

    public void createSignatureAndSerialize() throws IOException {
        createSignature();
        serialisationData();
        updateMessage("Parsing data...");
        updateProgress(ProgressBar.INDETERMINATE_PROGRESS, 0);
        MainFX.consummationParser = new ConsummationParser();
    }

    public void serialisationData() throws IOException {
        System.out.println("update processing");
        updateMessage("Update processing");
        DataPath.cereal.createNewFile();
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(DataPath.cereal))) {
            updateMessage("Delete success");
            System.out.println("delete success");
            updateMessage("Serializing... (this operation can took a moment)");
            MainFX.consummationParser = new ConsummationParser();
            oos.writeObject(MainFX.consummationParser);
            System.out.println(MainFX.consummationParser.getEnergyData().size());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Return the CRC32 store in the File checksum
     * @param checksum  The file containing the checksum
     * @return long The CRC32 of the File
     * @throws IOException  If an input or output exception occurred
     */
    public static long getFileCRC32(File checksum) throws IOException {
        long res = 0;
        if(checksum.exists()){
            FileReader fileReader = new FileReader(checksum);
            CharBuffer charBuffer = CharBuffer.allocate(32);
            if(fileReader.read(charBuffer) > 0) {
                res = Long.parseLong(charBuffer.rewind().toString().trim());
            }
        }
        return res;
    }

    /**
     * @param baseUrl   The url of the latest Data
     * @return long The checksum of the data at the base url
     * @throws IOException  If an input or output exception occurred
     */
    public long getRemoteCRC32(String baseUrl) throws IOException {
        CRC32 crc32 = new CRC32();
        URL url = new URL(baseUrl);
        URLConnection urlConnection = url.openConnection();
        Scanner sc = new Scanner(urlConnection.getInputStream());
        StringBuilder stringBuilder = new StringBuilder();
        while (sc.hasNext()){
            stringBuilder.append(sc.next());
        }
        crc32.update(stringBuilder.toString().getBytes());
        return crc32.getValue();
    }

    /**
     * @param file   The file to pass in the checksum function
     * @return long The checksum of the data at the base url
     * @throws IOException  If an input or output exception occurred
     */
    public long getRemoteCRC32(File file) throws IOException {
        System.out.println("Get CRC 32 of " + file.getName());
        updateMessage("Get CRC 32 of " + file.getName());
        CRC32 crc32 = new CRC32();
        Scanner sc = new Scanner(new FileInputStream(file));
        StringBuilder stringBuilder = new StringBuilder();
        while (sc.hasNext()){
            stringBuilder.append(sc.next());
        }
        crc32.update(stringBuilder.toString().getBytes());
        return crc32.getValue();
    }

    public void createSignature() throws IOException {
        DataPath.signature.createNewFile();
        System.out.println("Creating data.csv CRC32 signature");
        updateMessage("Creating data.csv CRC32 signature");
        FileWriter fileWriter = new FileWriter(DataPath.signature);
        fileWriter.write(String.valueOf(getRemoteCRC32(new File(DataPath.dataFile_path))));
        fileWriter.close();
        System.out.println("Update CRC32 kapi céréales");
        updateMessage("Update CRC32 kapi céréales");
    }

    /**
     * @param checksum  The file containing the checksum
     * @param baseUrl   The url of the latest Data
     * @return  boolean Return true if an update is available and false if not
     * @throws IOException  If an input or output exception occurred
     */
    public boolean isUpdateAvailable(File checksum, String baseUrl) throws IOException {
        boolean updateAvailable = true;
        if(checksum.exists()){
            if(getFileCRC32(checksum) == getRemoteCRC32(baseUrl)){
                updateAvailable = false;
            }
        }
        return updateAvailable;
    }

    /**
     * @param baseUrl   The url of the latest record
     * @param csvUrl    The url of the data
     * @param checksum  The file containing the checksum
     * @param dataPath  The path where the data should be saved
     * @throws IOException  If an input or output exception occurred
     */
    public void downloadUpdate(String baseUrl, String csvUrl, File checksum, String dataPath) throws IOException {
        BufferedInputStream bufferedInputStream = new BufferedInputStream(new URL(csvUrl).openStream());
        FileOutputStream fileOutputStream = new FileOutputStream(dataPath);
        byte[] dataBuffer = new byte[1024];
        int bytesRead;
        updateMessage("Downloading...");
        updateProgress(ProgressBar.INDETERMINATE_PROGRESS, 0);
        System.out.println("Telechargement en cours...");
        while((bytesRead = bufferedInputStream.read(dataBuffer, 0, 1024)) != -1){
            fileOutputStream.write(dataBuffer, 0, bytesRead);
        }
        fileOutputStream.flush();
        fileOutputStream.close();
        FileWriter fileWriter = new FileWriter(checksum);
        fileWriter.write(String.valueOf(getRemoteCRC32(baseUrl)));
        fileWriter.flush();
        fileWriter.close();
        updateMessage("Downloading finish");
        updateProgress(60, 80);
        System.out.println("Telechargement Termine");
    }
}