package fr.iutlan.preri.DataVisualization;

import fr.iutlan.preri.DataExploitation.Dot;
import fr.iutlan.preri.MainFX;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

import java.time.format.DateTimeFormatter;
import java.util.List;

public class DisplayGraph {
    public static LineChart<String, Number> lineChart(List<Dot> dotList, boolean energy_display, boolean temperature_display, boolean prediction_display){
        CategoryAxis xAxis = new CategoryAxis();
        NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("Date");
        yAxis.setLabel("Consommation Electrique (en MW/100)");
        LineChart<String, Number> lineChart = new LineChart<>(xAxis, yAxis);

        if(energy_display) {
            XYChart.Series<String, Number> consomax = new XYChart.Series<>();
            dotList.parallelStream().sorted(Dot::compareTo).forEachOrdered(dot -> {
                consomax.getData().add(new XYChart.Data<>(dot.getDate().format(DateTimeFormatter.ISO_LOCAL_DATE), dot.getMax() / 100));
            });
            consomax.setName("Consommation Maximale");
            lineChart.getData().add(consomax);

            XYChart.Series<String, Number> consomin = new XYChart.Series<>();
            dotList.parallelStream().sorted(Dot::compareTo).forEachOrdered(dot -> {
                consomin.getData().add(new XYChart.Data<>(dot.getDate().format(DateTimeFormatter.ISO_LOCAL_DATE), dot.getMin() / 100));
            });
            consomin.setName("Consommation Minimale");
            lineChart.getData().add(consomin);

            XYChart.Series<String, Number> consoavg = new XYChart.Series<>();
            dotList.parallelStream().sorted(Dot::compareTo).forEachOrdered(dot -> {
                if(dot.getAverage() > 0) {
                    consoavg.getData().add(new XYChart.Data<>(dot.getDate().format(DateTimeFormatter.ISO_LOCAL_DATE), dot.getAverage() / 100));
                }else{
                    consoavg.getData().add(new XYChart.Data<>(dot.getDate().format(DateTimeFormatter.ISO_LOCAL_DATE), dot.getAverage() / 100));
                }
            });
            consoavg.setName("Consommation Moyenne");
            lineChart.getData().add(consoavg);
        }
        if(temperature_display){
            XYChart.Series<String, Number> temperature = new XYChart.Series<>();
            dotList.parallelStream().sorted(Dot::compareTo).forEachOrdered(dot -> {
                temperature.getData().add(new XYChart.Data<>(dot.getDate().format(DateTimeFormatter.ISO_LOCAL_DATE), dot.getTemperature()));
            });
            temperature.setName("Temperature (en °C)");
            lineChart.getData().add(temperature);
        }
        if(prediction_display) {
            XYChart.Series<String, Number> predictions = new XYChart.Series<>();
            MainFX.consummationParser.getPredictions().forEach((date, aDouble) -> {
                predictions.getData().add(new XYChart.Data<>(date.format(DateTimeFormatter.ISO_LOCAL_DATE), aDouble / 100));
            });
            predictions.setName("Prediction de la consommation moyenne");
            lineChart.getData().add(predictions);
        }

        lineChart.setAnimated(true);
        lineChart.setTitle("Consommation en " + dotList.get(0).getName_region() + " du " + dotList.get(0).getDate() + " a " + dotList.get(dotList.size()-1).getDate());
        return lineChart;
    }
}
