package fr.iutlan.preri.DataVisualization;

public enum VisualizationRange {
    DAILY, WEEKLY, MONTHLY, ANNUALLY
}
