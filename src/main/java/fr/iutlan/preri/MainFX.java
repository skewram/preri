package fr.iutlan.preri;

import com.google.common.eventbus.EventBus;
import fr.iutlan.preri.DataExploitation.ConsummationParser;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Objects;

public class MainFX extends Application {
    public static ConsummationParser consummationParser;
    public static EventBus eventBusToMainController;

    public static void main(String[] args){
        System.out.println("                                                 \n" +
                " _|_|_|    _|_|_|    _|_|_|_|  _|_|_|    _|_|_|  \n" +
                " _|    _|  _|    _|  _|        _|    _|    _|    \n" +
                " _|_|_|    _|_|_|    _|_|_|    _|_|_|      _|    \n" +
                " _|        _|    _|  _|        _|    _|    _|    \n" +
                " _|        _|    _|  _|_|_|_|  _|    _|  _|_|_|  \n" +
                "                                                 ");

        eventBusToMainController = new EventBus();

        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("loading.fxml")));
        primaryStage.setTitle("Chargement de P.R.E.R.I");
        primaryStage.getIcons().add(new Image(String.valueOf(getClass().getClassLoader().getResource("icon.png"))));
        primaryStage.setResizable(false);
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        primaryStage.setScene(new Scene(root, 850, 350));
        primaryStage.sizeToScene();
        primaryStage.show();
    }

    public static void benchmark() throws FileNotFoundException {
        System.out.println("Benchark lancé");

        long startTime = Calendar.getInstance().getTimeInMillis();
        consummationParser = new ConsummationParser();
        System.out.println("Parsage des données: \t " + getDeltaFromNow(startTime));

        /*
        1 Day
         */

        startTime = Calendar.getInstance().getTimeInMillis();
        consummationParser.get("Bretagne", LocalDate.of(2021, 1, 1));
        System.out.println("1 Day (single core): \t" + getDeltaFromNow(startTime));
        startTime = Calendar.getInstance().getTimeInMillis();
        consummationParser.getMultiCore("Bretagne", LocalDate.of(2021, 1, 1));
        System.out.println("1 Day (multi core): \t" + getDeltaFromNow(startTime));

        /*
        1 Week
         */

        startTime = Calendar.getInstance().getTimeInMillis();
        for(LocalDate date = LocalDate.of(2021, 1, 1); date.isBefore(LocalDate.of(2021, 1, 7)); date = date.plusDays(1)){
            consummationParser.get("Bretagne", date);
        }
        System.out.println("1 Week (single core): \t" + getDeltaFromNow(startTime));
        startTime = Calendar.getInstance().getTimeInMillis();
        for(LocalDate date = LocalDate.of(2021, 1, 1); date.isBefore(LocalDate.of(2021, 1, 7)); date = date.plusDays(1)){
            consummationParser.getMultiCore("Bretagne", date);
        }
        System.out.println("1 Week (multi core): \t" + getDeltaFromNow(startTime));

        /*
        1 Month
         */

        startTime = Calendar.getInstance().getTimeInMillis();
        for(LocalDate date = LocalDate.of(2021, 1, 1); date.isBefore(LocalDate.of(2021, 2, 1)); date = date.plusDays(1)){
            consummationParser.get("Bretagne", date);
        }
        System.out.println("1 Month (single core): \t" + getDeltaFromNow(startTime));
        startTime = Calendar.getInstance().getTimeInMillis();
        for(LocalDate date = LocalDate.of(2021, 1, 1); date.isBefore(LocalDate.of(2021, 2, 1)); date = date.plusDays(1)){
            consummationParser.getMultiCore("Bretagne", date);
        }
        System.out.println("1 Month (multi core): \t" + getDeltaFromNow(startTime));

        /*
        1 Year
         */

        startTime = Calendar.getInstance().getTimeInMillis();
        for(LocalDate date = LocalDate.of(2020, 1, 1); date.isBefore(LocalDate.of(2021, 1, 1)); date = date.plusDays(1)){
            consummationParser.get("Bretagne", date);
        }
        System.out.println("1 Year (single core): \t" + getDeltaFromNow(startTime));
        startTime = Calendar.getInstance().getTimeInMillis();
        for(LocalDate date = LocalDate.of(2020, 1, 1); date.isBefore(LocalDate.of(2021, 1, 1)); date = date.plusDays(1)){
            consummationParser.getMultiCore("Bretagne", date);
        }
        System.out.println("1 Year (multi core): \t" + getDeltaFromNow(startTime));


        System.out.println("Fin du benchmark");
    }

    public static String getDeltaFromNow(long startTime){
        return (Calendar.getInstance().getTimeInMillis() - startTime) + "ms";
    }
}
