package fr.iutlan.preri.Controllers;

import com.google.common.eventbus.Subscribe;
import fr.iutlan.preri.DataVisualization.DisplayGraph;
import fr.iutlan.preri.DataVisualization.VisualizationRange;
import fr.iutlan.preri.MainFX;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import javax.swing.event.ChangeEvent;
import java.net.URL;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Objects;
import java.util.ResourceBundle;


public class MainController implements Initializable {
    @FXML private Button btnValider;    //Valider
    @FXML private MenuButton menuZone;  //Bouton menu
    @FXML private DatePicker datepickerD;    //Calendriers
    @FXML private DatePicker datepickerF;
    @FXML private CheckBox c1;              //Toutes les checkbox
    @FXML private CheckBox c2;
    @FXML private CheckBox predcb;
    @FXML private RadioButton rbjour;
    @FXML private RadioButton rbweek;
    @FXML private RadioButton rbmois;
    @FXML private RadioButton rbannee;
    @FXML private VBox graphcontainer;

    Alert alert = new Alert(Alert.AlertType.ERROR, "La date de fin ne peut pas etre inferieur a la date de debut ", ButtonType.OK);

    double x,y;                 //pos de la fenetre

    private LocalDate dateD;     //type de la date retournée par les calendriers
    private LocalDate dateF;

    private boolean c1Test;     //booléens des checkbox
    private boolean c2Test;
    private boolean pred;

    private String ville;       //String de la région

    private double optnAlpha = 0.5;      //résultat fenêtre option
    private double optnBeta = 0.5;
    private double optnGamma = 0.5;

    private VisualizationRange intervalle = VisualizationRange.DAILY;        //booléens des radio button (1 seul true)

    public void intervalle() {
        if (rbjour.isSelected()) intervalle = VisualizationRange.DAILY;
        else if (rbweek.isSelected()) intervalle = VisualizationRange.WEEKLY;
        else if (rbmois.isSelected()) intervalle = VisualizationRange.MONTHLY;
        else if (rbannee.isSelected()) intervalle = VisualizationRange.ANNUALLY;
    }   //reccup la donnée du radio button

    public void handleDatePickerAction(ActionEvent e) {
        DatePicker x = (DatePicker) e.getSource();
        if (x.getId().equals(datepickerD.getId())) dateD = x.getValue();
        else if(x.getId().equals(datepickerF.getId())) dateF = x.getValue();
        if(dateF != null && dateD != null) {
            if (dateF.isBefore(dateD)) {
                alert.showAndWait();
                if (alert.getResult() == ButtonType.OK) alert.close();
            }
        }

        btnValider.setDisable(ville == null || dateD == null || dateF == null || (!c1Test && !c2Test && !pred) || (dateF.isBefore(dateD)));
    }   //reccup la donnée du calendrier

    public void Valider() {
        if(c1Test) System.out.println("CheckBox N°1 : True");
        else System.out.println("Checkbox N°1 : False");

        if(c2Test) System.out.println("CheckBox N°2 : True");
        else System.out.println("Checkbox N°2 : False");

        if(pred) System.out.println("CheckBox N°3 : True");
        else System.out.println("Checkbox N°3 : False");

        System.out.println("Date :"+dateD+" "+dateF+"\nVille :" +ville);
        LineChart<String, Number> chart;
        switch (intervalle){
            case DAILY:
                MainFX.consummationParser.updateForecast(ville, optnAlpha, optnBeta, optnGamma, (int) ChronoUnit.DAYS.between(dateD, dateF), 1, dateF, dateF.plusWeeks(8), intervalle);
                chart = DisplayGraph.lineChart(MainFX.consummationParser.getMultiCoreDaily(ville, dateD, dateF), c1Test, c2Test, pred);
                break;
            case WEEKLY:
                MainFX.consummationParser.updateForecast(ville, optnAlpha, optnBeta, optnGamma, (int) ChronoUnit.DAYS.between(dateD, dateF), 7, dateF, dateF.plusMonths(6), intervalle);
                chart = DisplayGraph.lineChart(MainFX.consummationParser.getMultiCoreWeek(ville, dateD, dateF), c1Test, c2Test, pred);
                break;
            case MONTHLY:
                MainFX.consummationParser.updateForecast(ville, optnAlpha, optnBeta, optnGamma, (int) ChronoUnit.DAYS.between(dateD, dateF), 4, dateF, dateF.plusYears(1), intervalle);
                chart = DisplayGraph.lineChart(MainFX.consummationParser.getMultiCoreMonth(ville, dateD, dateF), c1Test, c2Test, pred);
                break;
            case ANNUALLY:
                MainFX.consummationParser.updateForecast(ville, optnAlpha, optnBeta, optnGamma, (int) ChronoUnit.DAYS.between(dateD, dateF), 10, dateF, dateF.plusYears(10), intervalle);
                chart = DisplayGraph.lineChart(MainFX.consummationParser.getMultiCoreAnnual(ville, dateD, dateF), c1Test, c2Test, pred);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + intervalle);
        }
        VBox.setVgrow(chart, Priority.ALWAYS);
        graphcontainer.getChildren().clear();
        graphcontainer.getChildren().add(chart);
    }   //gère le bouton "valider" et lance le programme

    public void Mizone(ActionEvent e) {
        ville = ((MenuItem) e.getSource()).getText();
        menuZone.setText(ville);

        btnValider.setDisable(ville == null || dateD == null || dateF == null || (!c1Test && !c2Test && !pred) || (dateF.isBefore(dateD)));
    }   //gère le menu button de la sélection de zone

    public void checkbox() {
        c1Test = c1.isSelected();
        c2Test = c2.isSelected();
        pred   = predcb.isSelected();
        btnValider.setDisable(ville == null || dateD == null || dateF == null || (!c1Test && !c2Test && !pred) || (dateF.isBefore(dateD)));
    }   //gère les checkbox en booléen

    public void optnAV() {
        try {
            Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("option.fxml")));
            Stage stage = new Stage();
            stage.initStyle(StageStyle.TRANSPARENT);
            stage.setScene(new Scene(root));
            stage.show();
        } catch (Exception e) {
            System.out.println("fenetre optn avancee marche pas");
        }
    } //fenetre option

    //fenêtre custom
    @FXML void draged(MouseEvent event) {
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setX(event.getScreenX() -x);
        stage.setY(event.getScreenY() -y);
    }

    @FXML void pressed(MouseEvent event) {
        x = event.getSceneX();
        y = event.getSceneY();
    }

    @FXML void min(MouseEvent event) {
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setIconified(true);
    }

    @FXML void max(MouseEvent event) {
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setFullScreen(true);
    }

    @FXML void close(MouseEvent event) {
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.close();
    }

    @Subscribe public void recordHyperParameterChange(ChangeEvent e){
        Double[] res = (Double[]) e.getSource();
        optnAlpha = res[0];
        optnBeta = res[1];
        optnGamma = res[2];
        System.out.println("Changed alpha to " + res[0] + " beta to " + res[1] + " gamma to " + res[2]);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        optnAlpha = 0.5;
        optnBeta = 0.5;
        optnGamma = 0.5;
        MainFX.eventBusToMainController.register(this);
    }
}
