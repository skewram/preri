package fr.iutlan.preri.Controllers;

import fr.iutlan.preri.Update;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;

public class LoadingController implements Initializable {
    @FXML private Label statustext;
    @FXML private ProgressBar progressbar;
    @FXML private ImageView logo;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        logo.setImage(new Image(Objects.requireNonNull(getClass().getClassLoader().getResource("icon.png")).toString()));
        progressbar.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
        statustext.setText("Initializing...");
        Task<Boolean> updateTask = new Update();

        statustext.textProperty().bind(updateTask.messageProperty());
        progressbar.progressProperty().bind(updateTask.progressProperty());

        updateTask.setOnSucceeded(event -> {
            statustext.textProperty().unbind();
            progressbar.progressProperty().unbind();
            progressbar.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
            try {
                statustext.setText("Launching window...");
                Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("main.fxml")));
                Stage stage = new Stage();
                stage.setTitle("PRERI");
                stage.getIcons().add(new Image(String.valueOf(getClass().getClassLoader().getResource("icon.png"))));
                stage.setMinWidth(1200);
                stage.setMinHeight(720);
                stage.setWidth(1200);
                stage.setHeight(720);
                stage.initStyle(StageStyle.TRANSPARENT);
                stage.setScene(new Scene(root, 300, 275));
                stage.show();
                ((Stage) statustext.getScene().getWindow()).close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        new Thread(updateTask).start();
    }
}
