package fr.iutlan.preri.Controllers;

import fr.iutlan.preri.MainFX;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import javax.swing.event.ChangeEvent;
import java.net.URL;
import java.util.Collections;
import java.util.ResourceBundle;

public class OptionController implements Initializable {
    @FXML private TextField alpha;          //Option avancée
    @FXML private TextField beta;
    @FXML private TextField gamma;
    @FXML private Label LabelError;

    private double optnAlpha;
    private double optnBeta;
    private double optnGamma;

    double x,y;

    public void ValiderOptn(ActionEvent event) {
        ObservableList<String> alphaStyle = alpha.getStyleClass();
        ObservableList<String> betaStyle = beta.getStyleClass();
        ObservableList<String> gammaStyle = gamma.getStyleClass();
        if (Double.parseDouble(alpha.getText()) >= 0 && Double.parseDouble(alpha.getText()) <= 1) {
            optnAlpha = Double.parseDouble(alpha.getText());
            if (alphaStyle.contains("error")) alphaStyle.removeAll(Collections.singleton("error"));
        } else {
            if (!alphaStyle.contains("error")) {
                alphaStyle.add("error");
                LabelError.setVisible(true);
            }
        }
        if (Double.parseDouble(beta.getText()) >= 0 && Double.parseDouble(beta.getText()) <= 1) {
            optnBeta = Double.parseDouble(beta.getText());
            if (betaStyle.contains("error")) betaStyle.removeAll(Collections.singleton("error"));
        } else {
            if (!betaStyle.contains("error")) {
                betaStyle.add("error");
                LabelError.setVisible(true);
            }
        }
        if (Double.parseDouble(gamma.getText()) >= 0 && Double.parseDouble(gamma.getText()) <= 1) {
            optnGamma = Double.parseDouble(gamma.getText());
            if (gammaStyle.contains("error")) gammaStyle.removeAll(Collections.singleton("error"));
        } else {
            if (!gammaStyle.contains("error")) {
                gammaStyle.add("error");
                LabelError.setVisible(true);
            }
        }
        if (Double.parseDouble(alpha.getText()) < 0 || Double.parseDouble(alpha.getText()) > 1 || Double.parseDouble(beta.getText()) < 0 || Double.parseDouble(beta.getText()) > 1 || Double.parseDouble(gamma.getText()) < 0 || Double.parseDouble(gamma.getText()) > 1) {

        } else {
            MainFX.eventBusToMainController.post(new ChangeEvent(new Double[]{optnAlpha, optnBeta, optnGamma}));
            Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
            stage.close();
        }
    }   //resultat valider option

    @FXML
    void draged(MouseEvent event) {
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setX(event.getScreenX() -x);
        stage.setY(event.getScreenY() -y);
    }

    @FXML void pressed(MouseEvent event) {
        x = event.getSceneX();
        y = event.getSceneY();
    }

    @FXML void close(MouseEvent event) {
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.close();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        optnAlpha = 0.5;
        optnBeta = 0.5;
        optnGamma = 0.5;
        this.alpha.setText(String.valueOf(optnAlpha));
        this.beta.setText(String.valueOf(optnBeta));
        this.gamma.setText(String.valueOf(optnGamma));
    }
}
